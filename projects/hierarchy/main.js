'use strict';

const assert = require('assert');

await O.addStyle('style.css');

const proj = O.project;
const mainDiv = O.ceDiv(O.body, 'main');
// const termDiv = O.ceDiv(mainDiv, 'term');
const buttonsDiv = O.ceDiv(mainDiv, 'buttons');
const treeDiv = O.ceDiv(mainDiv, 'tree');

let tree;
let terms;

const main = () => {
  O.dbgAssert = 1;
  
  loadTree();
  showTree();
  
  const newTermBnt = mkBtn('New term', evt => {
    const term = prompt();
    if(term === null) return;
    if(hasTerm(term, 1)) return;
    
    let nodePrev = null;
    let indexPrev = null;
    let node = tree;
    
    while(1){
      if(node === null){
        if(nodePrev === null){
          tree = [term];
        }else{
          nodePrev[indexPrev] = [term];
        }
        
        addTerm(term);
        updateTree();
        // clearTerm();
        return;
      }
      
      const name = node[0];
      const len = node.length;
      
      const r = name === 'term' ? '>' : prompt(name);
      
      if(r === null){
        // clearTerm();
        return;
      }
      
      if(r === '<'){
        if(nodePrev === null){
          tree = [term, tree];
        }else{
          nodePrev[indexPrev] = [term, node];
        }
        
        addTerm(term);
        updateTree();
        // clearTerm();
        return;
      }
      
      if(r === '>'){
        nodePrev = node;
        
        if(len === 1){
          node.push(null);
          indexPrev = 1;
          node = null;
          continue;
        }
        
        if(len === 2){
          indexPrev = 1;
          node = node[1];
          continue;
        }
        
        if(len === 3){
          const t1 = node[1];
          const t2 = node[2];
          
          const r = prompt(`${t1[0]}, ${t2[0]}`);
          if(r === null) return;
          
          if(r === '<'){
            indexPrev = 1;
            node = node[1];
            continue;
          }
          
          if(r === '>'){
            indexPrev = 2;
            node = node[2];
            continue;
          }
          
          assert(r !== '');
          
          const t = r;
          if(hasTerm(t, 1)) return;
          
          node[1] = [t, t1, t2];
          node[2] = [term];
          
          addTerm(term);
          updateTree();
          // clearTerm();
          return;
        }
        
        assert.fail();
      }
      
      if(r === ''){
        assert(nodePrev !== null);
        assert(nodePrev.length === 2);

        nodePrev.push([term]);
          
        addTerm(term);
        updateTree();
        // clearTerm();
        return;
      }
      
      const t = r;
      if(hasTerm(t, 1)) return;
      
      if(nodePrev === null){
        tree = [t, tree, [term]];
      }else{
        nodePrev[indexPrev] = [t, node, [term]];
      }
      
      addTerm(term);
      updateTree();
      // clearTerm();
      return;
    }
  
    assert.fail();
  });
  
  // showTree(tree);
};

// const setTerm = (term='') => {
//   termDiv.innerText = term;
// };
// 
// const clearTerm = () => {
//   setTerm();
// };

const mkBtn = (label, f) => {
  return O.ceButton(buttonsDiv, label, '', f);
};

const initTree = () => {
  tree = null;
};

const loadTree = () => {
  if(!O.has(localStorage, proj)){
    initTree();
    saveTree();
    terms = new Set();
    return;
  }
  
  tree = JSON.parse(localStorage[proj]);
  terms = getTerms();
};

const saveTree = () => {
  localStorage[proj] = JSON.stringify(tree);
};

const showTree = () => {
  treeDiv.innerText = '';
  
  const showNode = function*(elem, node){
    if(node === null) return;
    
    const len = node.length;
    const name = node[0];
    
    if(len === 1){
      const span = O.ce(elem, 'span', 'term');
      span.innerText = name;
      return;
    }
    
    const details = O.ce(elem, 'details');
    const summary = O.ce(details, 'summary');
    
    summary.innerText = name;
    
    if(len === 3){
      const t1 = node[1];
      const t2 = node[2];
      
      if(t1[0] > t2[0]){
        node[1] = t2;
        node[2] = t1;
      }
    }
    
    for(let i = 1; i !== len; i++)
      yield [showNode, details, node[i]];
  };
  
  O.rec(showNode, treeDiv, tree);
};

const updateTree = () => {
  showTree();
  saveTree();
  O.show(treeDiv);
};

const getTerms = () => {
  const terms = new Set();
  
  const getTermsForNode = function*(node){
    if(node === null) return;
    
    const len = node.length;
    terms.add(node[0]);
    
    for(let i = 1; i !== len; i++)
      yield [getTermsForNode, node[i]];
  };
  
  O.rec(getTermsForNode, tree);
  
  return terms;
};

const hasTerm = (term, showAlert=0) => {
  const has = terms.has(term);
  
  if(showAlert && has)
    alert(`Term ${O.sf(term)} already exists`);
  
  return has;
};

const addTerm = term => {
  terms.add(term);
};

main();