'use strict';

const assert = require('assert');

const {
  min, max,
  floor, ceil, round,
  sqrt,
  sin, cos,
} = Math;

const {pi, pi2, pih} = O;

const radius = .9;
const mult = 1;

const cols = [
  [0, 255, 255],
  [255, 0, 255],
];

const {g} = O.ceCanvas();
const {canvas} = g;

let iw, ih, iwh, ihh;

const main = () => {
  aels();
  onResize();
};

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  render();
};

const render = () => {
  g.fillStyle = 'white';
  g.fillRect(0, 0, iw, ih);
  
  const m = min(iw, ih);
  const mh = m / 2;
  const rad = mh * radius;
  const diam = rad * 2;
  const area = rad ** 2 * pi;
  
  const s = ceil(diam);
  const s1 = s - 1;
  const ptsNum = s ** 2 * mult;
  const grid = new O.Grid(s, s, () => 0);
  
  const mkCoord = a => {
    return O.bound(round(a), 0, s1);
  };
  
  for(let i = 0; i !== ptsNum; i++){
    const angle = rand() * pi2;
    const r = sqrt(rand()) * rad;
    const x = mkCoord(rad + r * cos(angle));
    const y = mkCoord(rad + r * sin(angle));
    const n = grid.get(x, y);
    
    grid.set(x, y, n + 1);
  }
  
  const d = new O.ImageData(g);
  const col = new Uint8ClampedArray(3);
  const [c1, c2] = cols;
  
  d.iter((cx, cy, d) => {
    const xx = cx - iwh + rad;
    const yy = cy - ihh + rad;
    
    if(O.dist(xx, yy, rad, rad) > rad)
      return;
    
    const x = mkCoord(xx);
    const y = mkCoord(yy);
    const n = grid.get(x, y);
    const k2 = n / mult;
    const k1 = 1 - k2;
    
    for(let i = 0; i !== 3; i++)
      col[i] = c1[i] * k1 + c2[i] * k2;
    
    return col;
  });
  
  d.put();
  
  // g.fillStyle = 'black';
  // g.beginPath();
  // g.arc(iwh, ihh, rad, 0, pi2);
  // g.fill();
};

const rand = () => {
  return O.randf();
};

main();