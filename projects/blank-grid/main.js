'use strict';

const assert = require('assert');

const w = 5;
const h = 5;
const size = 60;

const cols = {
  canvasBg: 'darkgray',
};

const {g} = O.ceCanvas(1);
const {canvas} = g;

let iw, ih;
let iwh, ihh;

let grid = null;

const main = () => {
  initGrid();
  aels();
  onResize();
};

const initGrid = () => {
  grid = new O.Grid(w, h, (x, y) => {
    return x + y & 1;
  });
};

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  render();
};

const render = () => {
  g.fillStyle = cols.canvasBg;
  g.fillRect(0, 0, iw, ih);
  
  g.translate(iwh, ihh);
  g.scale(size);
  g.translate(-w / 2, -h / 2);
  
  const {gs} = g;
  
  grid.iter((x, y, d) => {
    g.save();
    g.translate(x, y);
    
    g.fillStyle = d ? '#888' : '#fff';
    g.fillRect(0, 0, 1, 1);
    
    g.restore();
  });
  
  g.beginPath();
  for(let y = 0; y <= h; y++){
    g.moveTo(0, y);
    g.lineTo(w + gs, y);
  }
  for(let x = 0; x <= w; x++){
    g.moveTo(x, 0);
    g.lineTo(x, h + gs);
  }
  g.stroke();
  
  g.resetTransform();
};

main();