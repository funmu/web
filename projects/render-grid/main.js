'use strict';

const {
  min, max,
  floor, ceil, round,
} = Math;

const s = O.urlParam('s', 40) | 0;
const bgCol = O.urlParam('bg', 'white');

const N = null;

const {g} = O.ceCanvas(1);
const {canvas} = g;

let iw, ih;
let iwh, ihh;

const gridRaw = O.sanl(`
~~~~~~~#.~
~#...#~.~~
~#~.~~.#~~
~.~#.#.~~~
~#~~~~#.~~
#~.~#~.~~~
.~#~~.#.~~
~#.~.~~~~~
~.~~~~...~
~~~~~~~~~~
`).slice(1, -1).map((a, y, c) => O.ca(c.length, x => {
  const c = a[x];
  if(c === '.') return 0;
  if(c === '#') return 1;
  return null;
}));

const w = gridRaw[0].length;
const h = gridRaw.length;

const grid = new O.Grid(w, h, (x, y) => {
  return gridRaw[y][x];
});

const moves = [];

const main = () => {
  aels();
  onResize();
};

const aels = () => {
  O.ael('keydown', onKeyDown);
  O.ael('mousedown', onMouseDown);
  O.ael('resize', onResize);
  O.ael('contextmenu', onContextMenu);
};

const onKeyDown = evt => {
  const {ctrlKey, shiftKey, altKey, code} = evt;
  const flags = (ctrlKey << 2) | (shiftKey << 1) | altKey;
  
  if(flags === 4){
    if(code === 'KeyZ'){
      if(moves.length === 0) return;
      
      const [x, y] = moves.pop();
      grid.set(x, y, null);
      render();
      
      return;
    }
    
    return;
  }
};

const onMouseDown = evt => {
  const x = floor((evt.clientX - (iw - w * s) / 2) / s);
  const y = floor((evt.clientY - (ih - h * s) / 2) / s);
  if(!grid.includes(x, y)) return;
  
  const {button} = evt;
  const v = button === 0 ? 1 : button === 2 ? 0 : null;
  if(v === null) return;
  
  const vPrev = grid.get(x, y);
  if(vPrev !== null) return;
  
  grid.set(x, y, v);
  moves.push([x, y]);
  
  render();
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  render();
};

const onContextMenu = evt => {
  O.pd(evt);
};

const render = () => {
  g.fillStyle = bgCol
  g.fillRect(0, 0, iw, ih);

  g.translate((iw - w * s) / 2, (ih - h * s) / 2);
  g.scale(s);

  g.font(s * .5);

  const drawTile = (x, y, d) => {
    g.strokeStyle = '#808080';
    g.fillStyle = d === null ?
      'rgb(200,191,231)' :
      d ? 'black' : 'white';
    
    g.beginPath();
    g.rect(0, 0, 1, 1);
    g.fill();
    g.stroke();
  };

  grid.iter((x, y, d) => {
    g.save();
    g.translate(x, y);
    drawTile(x, y, d);
    g.restore();
  });
  
  g.resetTransform();
};

main();