'use strict';

const assert = require('assert');
const LS = require('./local-strings');

const {baseURL, proj, body} = O;

await O.addStyle('style.css');

const showResetDb = 0;

const mainPage = 'mainMenu';
const page = O.urlParam('page', mainPage);

const dateOffset = 0;
const curDate = O.dateOffset(O.curDate(), dateOffset);

let db = null;

const main = () => {
  db = loadDb();
  
  const [title, func] = pages[page];
  if(title !== null) O.title(title);
  
  func();
  
  if(page !== mainPage){
    O.ce(O.body, 'hr');
    
    const linkPrev = O.ceLink(body, LS(19), '#', 'link');
    
    O.ael(linkPrev, 'click', evt => {
      O.pd(evt);
      history.back();
    });
    
    link(LS(22));
  }
};

const pages = {
  [mainPage]: [LS(1), () => {
    const ps = [
      'newTask',
      'tasks',
      'tasksByDate',
      'todaysTask',
      ...showResetDb ? ['reset'] : [],
    ];
    
    for(const p of ps)
      link(pages[p][0], p);
  }],
  
  'newTask': [LS(2), () => {
    const inpName = input(LS(5));
    const inpDetails = inputTa(LS(21));
    
    btn(LS(3), () => {
      const name = inpName.value.trim();
      const details = inpDetails.value.trim();
      
      if(hasTask(name)){
        alert(LS(6, O.sf(name)));
        return;
      }
      
      addTask(name, details);
      nav(mainPage);
    });
  }],
  
  'tasks': [LS(4), () => {
    const tasks = [...db.tasks]
    
    mkSearch(tasks, name => {
      return [name, 'task', {name: escape(name)}];
    });
  }],
  
  'tasksByDate': [LS(7), () => {
    let fstDate = null;
    
    for(const dateStr of db.tasksByDate.keys()){
      const date = O.readDate(dateStr);
      
      if(fstDate === null || O.dateLt(date, fstDate))
        fstDate = date;
    }
    
    const arr = [];
    
    if(fstDate !== null){
      let date = curDate;
      
      while(O.dateGe(date, fstDate)){
        arr.push(showTaskForDate(date));
        date = O.prevDate(date);
      }
    }
    
    mkSearch(arr);
  }],
  
  'todaysTask': [LS(8), () => {
    text(showTaskForDate(curDate, body));
    
    btn(LS(9), () => {
      const name = genTask(curDate);
      
      if(name === null){
        alert(LS(10));
        return;
      }
      
      setTaskForDate(curDate, name);
      location.reload();
    });
  }],
  
  'reset': [LS(11), () => {
    btn(LS(12), () => {
      if(cancel(LS(13))) return;
      resetDb();
      nav();
    });
  }],
  
  'task': [null, () => {
    const name = unescape(O.urlParam('name'));
    
    if(!hasTask(name)){
      O.title(LS(24, '404'));
      text(LS(23, O.sf(name)));
      return;
    }
    
    const details = getDetails(name);
    
    O.title(LS(14, O.sf(name)));
    
    const inpName = input(LS(5));
    const inpDetails = inputTa(LS(21));
    
    inpName.value = name;
    inpDetails.value = details;
    
    btn(LS(20), () => {
      const name1 = inpName.value.trim();
      const details1 = inpDetails.value.trim();
      
      let changed = 0;
      
      if(name1 !== name){
        if(hasTask(name1)){
          alert(LS(6, O.sf(name1)));
          return;
        }
        
        renameTask(name, name1);
        changed = 1;
      }
      
      if(details1 !== details){
        setDetails(name1, details1);
        changed = 1;
      }
      
      if(changed) nav('task', {name: name1});
    });
    
    btn(LS(15), () => {
      if(cancel(LS(16, O.sf(name)))) return;
      deleteTask(name);
      nav();
    });
  }],
};

const initDbInfo = () => {
  return {
    'tasks': [],
    'details': [],
    'tasksByDate': [[], []],
  };
};

const getDbInfo = () => {
  try{
    assert(O.has(localStorage, proj));
    return JSON.parse(localStorage[proj]);
  }catch(e){
    return initDbInfo();
  }
};

const loadDb = () => {
  const info = getDbInfo();
  
  const db = {
    tasks: new Set(info.tasks),
    tasksLc: new Set(info.tasks.map(a => a.toLowerCase())),
    details: info.details,
    tasksByDate: new Map(O.zip(...info.tasksByDate)),
  };
  
  return db;
};

const saveDb = () => {
  const tbd = ['keys', 'values'].map(m => {
    return [...db.tasksByDate[m]()];
  });
  
  const info = {
    'tasks': [...db.tasks],
    'details': db.details,
    'tasksByDate': tbd,
  };
  
  localStorage[proj] = JSON.stringify(info);
};

const resetDb = () => {
  delete localStorage[proj];
};

const hasTask = name => {
  return db.tasksLc.has(name.toLowerCase());
};

const addTask = (name, details) => {
  db.tasks.add(name);
  db.details.push(details);
  saveDb();
};

const renameTask = (from, to) => {
  const {tasks, tasksByDate: tbd} = db;
  const tasksArr = [...tasks];
  const index = tasksArr.indexOf(from);
  
  tasksArr[index] = to;
  db.tasks = new Set(tasksArr);
  
  for(const [k, v] of tbd)
    if(v === from) tbd.set(k, to);
  
  saveDb();
};

const getDetails = name => {
  const tasksArr = [...db.tasks];
  const index = tasksArr.indexOf(name);
  
  return db.details[index];
};

const setDetails = (name, details) => {
  const tasksArr = [...db.tasks];
  const index = tasksArr.indexOf(name);
  
  db.details[index] = details;
  saveDb();
};

const deleteTask = name => {
  const {tasks, tasksByDate: tbd} = db;
  const tasksArr = [...tasks];
  const index = tasksArr.indexOf(name);
  
  tasks.delete(name);
  
  for(const [date, name1] of tbd)
    if(name1 === name)
      tbd.delete(date);
  
  db.details.splice(index, 1);
  saveDb();
};

const genTask = curDate => {
  if(db.tasks.size === 0)
    return null;
  
  const tasks = new Map();
  
  const calcProb = dif => {
    return dif ** 2;
  };
  
  for(const [dateStr, name] of db.tasksByDate){
    const date = O.readDate(dateStr);
    const dif = O.dateDif(date, curDate);
    if(dif === 0) continue;
    
    assert(dif > 0);
    
    if(!tasks.has(name))
      tasks.set(name, 0);
    
    const val = tasks.get(name);
    tasks.set(name, val + 1 / calcProb(dif));
  }
  
  const unusedTasks = [...db.tasks].filter(name => {
    return !tasks.has(name);
  });
  
  if(unusedTasks.length !== 0)
    return O.randElem(unusedTasks);
  
  for(const [name, val] of tasks)
    tasks.set(name, 1 / val);
  
  return O.sample(tasks);
};

const getTaskForDate = date => {
  const dateStr = O.showDate(date);
  const tbd = db.tasksByDate;
  
  if(tbd.has(dateStr)) return tbd.get(dateStr);
  return null;
};

const setTaskForDate = (date, name) => {
  const dateStr = O.showDate(date);
  const tbd = db.tasksByDate;
  
  tbd.set(dateStr, name);
  saveDb();
};

const showTaskForDate = date => {
  const task = getTaskForDate(date);
  const taskStr = task !== null ? ` ${task}` : '-- /';
  
  return `${O.showDate(date)} -${taskStr}`;
};

const mkSearch = (arr, func=null) => {
  const inp = input(LS(18));
  const list = O.ceDiv(body, 'list', 'list');
  
  const update = () => {
    const str = inp.value.toLowerCase();
    const arr1 = arr.filter(a => a.toLowerCase().includes(str));
    
    list.innerText = '';
    
    for(const elem of arr1){
      const linkInfo = func !== null ? func(elem) : null;
      
      if(linkInfo === null){
        const span = text1(list, elem);
        span.classList.add('text');
        continue;
      }
      
      link1(list, ...linkInfo);
    }
  };
  
  O.ael(inp, 'input', update);
  update();
};

const link = (label, page, args) => {
  link1(body, label, page, args);
};

const link1 = (elem, label, page, args) => {
  const url = mkUrl(page, args);
  const link = O.ceLink(elem, label, url, 'link');
};

const btn = (label, func) => {
  return O.ceBtn(body, label, '', func);
};

const text = label => {
  return text1(body, label);
};

const text1 = (elem, label) => {
  const span = O.ce(elem, 'span');
  span.innerText = label;
  return span;
};

const input = label => {
  return input1('input', label);
};

const inputTa = label => {
  return input1('textarea', label);
};

const input1 = (tag, label) => {
  const inpWrap = O.ceDiv(body, 'input-wrapper');
  text1(inpWrap, `${label}: `);
  const inp = O.ce(inpWrap, tag, 'text');
  return inp;
};

const nav = (page, args) => {
  const url = mkUrl(page, args);
  location.href = url;
};

const mkUrl = (page=mainPage, args=null) => {
  const params = args !== null ? O.keys(args).map(a => `&${a}=${args[a]}`) : '';
  return `${baseURL}/?project=${proj}&page=${page}${params}`;
};

const cancel = action => {
  return !confirm(LS(17, action));
};

main();