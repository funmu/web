'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math;

const size = 700;
const areaThreshold = 1;

const cwd = __dirname;
const srcFile = path.join(cwd, 'src.txt');

const cols = {
  bg: 'white',
  line: 'black',
};

const {g} = O.ceCanvas();
const {canvas} = g;

let iw, ih;
let iwh, ihh;

let frac;

const main = async () => {
  canvas.style.position = 'absolute';
  
  const src = await O.rfs(srcFile, 1);
  // const src = generateSrc();
  
  frac = O.sanll(src).map(a => {
    return O.sanl(a).map(a => group(2, a.trim().split(/\s+/).map(a => Number(a))));
  });
  
  const f = a => {
    if(typeof a === 'number') return a;
    
    if(a.length === 2 && a.every(a => typeof a === 'number')){
      const x = (a[0] - 50) / 225;
      const y = (a[1] - 10) / 450;
      return [x, y];
    }
    
    return a.map(f);
  };
  
  // frac = f(frac);
  // log(JSON.stringify(frac))
  
  aels();
  onResize();
};

const generateSrc = () => {
  const lines = [[0, 0, 1, 0, 1, 1, 0, 1, 0, 0]];
  
  const viewports = O.ca(O.randInt(1, .5), () => {
    while(1){
      const vp = O.ca(6, () => O.randf(0, 1));
      const [x0, y0, x1, y1, x2, y2] = vp;
      const dx1 = x1 - x0;
      const dy1 = y1 - y0;
      const dx2 = x2 - x0;
      const dy2 = y2 - y0;
      
      if(calcArea(dx1, dy1, dx2, dy2) < 1)
        return vp;
    }
  });
  
  const src = [lines, viewports].map(a => a.map(a => a.join(' ')).join('\n')).join('\n\n');
  log(src);
  
  return src;
};

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = evt => {
  const ss = size;
  
  iw = ss;
  ih = ss;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  canvas.style.left = `${round(O.iw / 2)}px`;
  canvas.style.top = `${round(O.ih / 2)}px`;
  canvas.style.transform = `translate(${round(-ss / 2)}px, ${round(-ss / 2)}px)`;
  
  g.fillStyle = cols.bg;
  g.fillRect(0, 0, iw, ih);
  
  g.strokeStyle = cols.line;
  g.lineWidth = 1;
  
  render();
};

const render = () => {
  O.rec(draw, 0, 0, iw, 0, 0, ih);
};

const draw = function*(x0, y0, x1, y1, x2, y2){
  const dx1 = x1 - x0;
  const dy1 = y1 - y0;
  const dx2 = x2 - x0;
  const dy2 = y2 - y0;
  
  if(calcArea(dx1, dy1, dx2, dy2) < areaThreshold) return;
  
  const [p1, p2] = frac;
  
  const map = ([x, y]) => {
    return [
      x0 + dx1 * x + dx2 * y,
      y0 + dy1 * x + dy2 * y,
    ];
  };
  
  for(const p of p1){
    g.beginPath();
    
    for(const xy of p)
      g.lineTo(...map(xy));
    
    g.stroke();
  }
  
  for(const p of p2){
    const [[ax0, ay0], [ax1, ay1], [ax2, ay2]] = p.map(map);
    yield [draw, ax0, ay0, ax1, ay1, ax2, ay2];
  }
};

const group = (n, arr) => {
  const arrNew = [];
  
  while(arr.length !== 0)
    arrNew.push(arr.splice(0, n));
  
  return arrNew;
};

const calcArea = (dx1, dy1, dx2, dy2) => {
  return abs(dx1 * dy2 - dy1 * dx2);
};

main();