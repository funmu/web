'use strict';

const assert = require('assert');
const Tile = require('./tile');

class Grid extends O.Grid{
  constructor(w, h){
    super(w, h, (x, y) => new Tile(null, x, y));
    
    this.iter((x, y, d) => {
      d.grid = this;
    });
    
    this.head = [0, 0];
    this.cactus = [0, 0];
    this.goal = [0, 0];
  }
  
  save(){
    const info = [...this.head, ...this.cactus];
    
    this.iter((x, y, d) => {
      info.push(d.bodyH, d.bodyV);
    });
    
    return info;
  }
  
  load(info){
    const {head, cactus} = this;
    
    let i = 0;
    
    const get = () => {
      return info[i++];
    };
    
    head[0] = get();
    head[1] = get();
    cactus[0] = get();
    cactus[1] = get();
    
    this.iter((x, y, d) => {
      d.bodyH = get();
      d.bodyV = get();
    });
  }
  
  get won(){
    const {cactus, goal} = this;
    
    return (
      cactus[0] === goal[0] &&
      cactus[1] === goal[1]
    );
  }
}

module.exports = Grid;