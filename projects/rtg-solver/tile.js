'use strict';

const assert = require('assert');

class Tile{
  bodyH1 = 0;
  bodyV1 = 0;
  
  constructor(grid, x, y){
    this.grid = grid;
    this.x = x;
    this.y = y;
    
    this.wall = 0;
    this.pthH = 0;
    this.pthV = 0;
    this.bodyH = 0;
    this.bodyV = 0;
  }
  
  *adjs(){
    const {grid, x, y} = this;
    
    for(let dy = -1; dy <= 0; dy++){
      for(let dx = -1; dx <= 0; dx++){
        const d = grid.get(x + dx, y + dy);
        if(d === null) continue;
        
        yield d;
      }
    }
  }
  
  hasWall(){
    for(const d of this.adjs())
      if(d.wall) return 1;
    
    return 0;
  }
  
  hasBody(a=0){
    const {x, y} = this;
    
    for(const d of this.adjs()){
      if((a ? d.bodyH1 : d.bodyH) && d.y === y) return 1;
      if((a ? d.bodyV1 : d.bodyV) && d.x === x) return 1;
    }
    
    return 0;
  }
}

module.exports = Tile;