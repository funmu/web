'use strict';

const assert = require('assert');

const {
  min, max,
  sin, cos, atan2,
} = Math;

const {pi, pi2} = O;

const {g} = O.ceCanvas();
const {canvas} = g;

const circX = 0;
const circY = 0;
const circR = 300;

const circR2 = circR ** 2;

const ps = [];
const psInv = [];

let clicked = 0;
let cx, cy;

let iw, ih;
let iwh, ihh;

const main = () => {
  aels();
  onResize();
};

const aels = () => {
  O.ael('mousedown', onMouseDown);
  O.ael('mousemove', onMouseMove);
  O.ael('mouseup', onMouseUp);
  O.ael('resize', onResize);
};

const onMouseDown = evt => {
  const {button} = evt;
  
  if(button === 0){
    if(clicked) return;
    
    clicked = 1;
    updateCur(evt);
    pushLine();
    pushPoint(cx, cy);
    
    return;
  }
};

const onMouseMove = evt => {
  if(!clicked) return;
  
  const cxPrev = cx;
  const cyPrev = cy;
  
  updateCur(evt);
  pushPoint(cx, cy);
  
  g.translate(iwh, ihh);
  
  g.strokeStyle = '#fff';
  g.beginPath();
  g.moveTo(cxPrev, cyPrev);
  g.lineTo(cx, cy);
  g.stroke();
  
  const arr = O.last(psInv);
  const n = arr.length;

  g.strokeStyle = '#488';
  g.beginPath();
  g.moveTo(arr[n - 4], arr[n - 3]);
  g.lineTo(arr[n - 2], arr[n - 1]);
  g.stroke();
  
  g.resetTransform(iwh, ihh);
};

const onMouseUp = evt => {
  const {button} = evt;
  
  if(button === 0){
    if(!clicked) return;
    
    clicked = 0;
    
    if(O.last(ps).length === 2)
      popLine();
    
    return;
  }
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  canvas.width = iw;
  canvas.height = ih;
  
  render();
};

const render = () => {
  g.fillStyle = '#000';
  g.fillRect(0, 0, iw, ih);
  
  g.translate(iwh, ihh);
  
  g.lineWidth = 3;
  g.strokeStyle = '#0ff';
  g.beginPath();
  g.arc(circX, circY, circR, 0, pi2);
  g.stroke();
  g.lineWidth = 1;
  
  g.strokeStyle = '#fff';
  for(const arr of ps){
    g.beginPath();
    for(let i = 0; i !== arr.length; i += 2)
      g.lineTo(arr[i], arr[i + 1]);
    g.stroke();
  }
  
  g.strokeStyle = '#488';
  for(const arr of psInv){
    g.beginPath();
    for(let i = 0; i !== arr.length; i += 2)
      g.lineTo(arr[i], arr[i + 1]);
    g.stroke();
  }
  
  g.resetTransform();
};

const pushLine = () => {
  ps.push([]);
  psInv.push([]);
};

const popLine = () => {
  ps.pop();
  psInv.pop();
};

const pushPoint = (x, y) => {
  const psArr = O.last(ps);
  const psInvArr = O.last(psInv);
  
  psArr.push(x, y);
  
  const angle = atan2(y - circY, x - circX);
  const dist = O.dist(circX, circY, x, y);
  const dist1 = circR2 / dist;
  const x1 = circX + dist1 * cos(angle);
  const y1 = circY + dist1 * sin(angle);
  
  psInvArr.push(x1, y1);
};

const updateCur = evt => {
  cx = evt.clientX - iwh;
  cy = evt.clientY - ihh;
};

main();