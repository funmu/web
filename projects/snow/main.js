'use strict';

const assert = require('assert');
const distribs = require('./distribs');

const {
  min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math;

const {pi, pih, pi2} = O;

const w = 640;
const h = 480;

const wh = w / 2;
const hh = h / 2;
const w1 = w - 1;
const h1 = h - 1;

const bg = O.ceCanvas(1).g;

const distrib = distribs.exp(100, 1.2);

let iw, ih, iwh, ihh;
let g, canvas;
let imgd;
let grid;

const cols = {
  sky: [0, 128, 255],
  wall: [0, 0, 0],
  snow: [255, 255, 255],
};

for(const key of O.keys(cols))
  cols[key] = new Uint8ClampedArray(cols[key]);

const col = new Uint8ClampedArray(3);

const main = () => {
  O.dbgAssert = 1;
  
  canvas = O.doc.createElement('canvas');
  canvas.width = w;
  canvas.height = h;
  g = canvas.getContext('2d');
  g.fillStyle = 'white';
  g.fillRect(0, 0, w, h);
  imgd = new O.ImageData(g);
  
  grid = new O.Grid(w, h, (x, y) => {
    return new Tile(x, y);
  });
  
  grid.iter((x, y, d) => {
    if(abs(x - wh) < 100 && h - y < 100){
      d.setWall();
      return;
    }
    
    if(h - y < 10){
      d.setWall();
      return;
    }
  });
  
  col.fill(255);
  
  updateSnow();
  
  aels();
  onResize();
};

class Tile{
  constructor(x, y){
    this.x = x;
    this.y = y;
    this.solid = 0;
    this.wall = 0;
    this.snow = 0;
    this.acc = 0;
    
    imgd.set(x, y, cols.sky); 
  }
  
  setWall(){
    const {x, y} = this;
    
    this.wall = 1;
    this.snow = 0;
    this.solid = 1;
    this.acc = 0;
    
    imgd.set(x, y, cols.wall);
  }
  
  setSnow(){
    const {x, y} = this;
    
    const d1 = grid.get(x, y + 1);
    assert(d1 !== null);
    assert(d1.solid);
    
    this.snow = 1;
    this.wall = 0;
    this.solid = 1;
    
    imgd.set(x, y, cols.snow);
  }
}

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  bg.resize(iw, ih);
  
  render();
};

const render = () => {
  const g = bg;
  
  g.fillStyle = 'darkgray';
  g.fillRect(0, 0, iw, ih);
  
  const x0 = round(iwh - wh);
  const y0 = round(ihh - hh);
  
  g.drawImage(canvas, x0, y0);
  
  g.beginPath();
  g.rect(x0, y0, w, h);
  g.stroke();
};

const updateSnow = () => {
  for(let i = 0; i !== 200; i++){
    // grid.iter((x, y, d) => {
    //   if(d.snow) d.setWall();
    // });
    
    updateSnow1();
  }
};

const updateSnow1 = () => {
  const dn = distrib.length;
  const k = 1;
  
  mainLoop: for(let xx = 0; xx !== w; xx++){
    let x = xx;
    let y = 0;
    
    while(!grid.get(x, y).solid)
      if(++y === h) continue mainLoop;
    
    y--;
    
    const d = grid.get(x, y);
    d.acc += distrib[0] * k;
    
    const ms = [[d, -1], [d, 1]];
    
    for(let i = 1; i !== dn; i++){
      const di = distrib[i] * k;
      
      const put = d => {
        while(1){
          const d1 = grid.get(d.x, d.y + 1);
          if(d1 === null) return;
          if(d1.solid) break;
          
          d = d1;
        }
        
        d.acc += di;
      };
      
      for(const m of ms){
        const d = m[0];
        const dir = m[1];
        
        if(d === null) continue;
        
        const d1 = grid.get(d.x + dir, y);
        
        if(d1 === null){
          m[0] = null;
          continue;
        }
        
        if(d1.solid){
          put(d);
          // m[1] = -dir;
          continue;
        }
        
        m[0] = d1;
        put(d);
      }
    }
  }
  
  grid.iter((x, y, d) => {
    let {acc} = d;
    if(acc === 0) return;
    
    d.acc = 0;
    
    while(d.solid)
      if(--y === 0 || (d = grid.get(x, y)).wall)
        return;
    
    while(acc > 1){
      d.setSnow();
      acc--;
      
      if(--y === 0 || (d = grid.get(x, y)).solid)
        return;
    }
    
    if(O.randf() < acc)
      d.setSnow();
  });
  
  imgd.put();
};

main();