'use strict';

const assert = require('assert');

const {
  PI, min, max, abs, floor,
  ceil, round, sqrt, sin, cos,
  atan, atan2, clz32,
  log2,
} = Math;

const {pi, pih, pi2} = O;

const {g} = O.ceCanvas();
const {canvas} = g;

const col = new Uint8ClampedArray(3);

let w, h, wh, hh;
let data;

const main = () => {
  aels();
  onResize();
};

const aels = () => {
  O.ael('resize', onResize);
};

const onResize = evt => {
  w = O.iw;
  h = O.ih;
  wh = w / 2;
  hh = h / 2;
  
  canvas.width = w;
  canvas.height = h;
  
  g.fillStyle = 'black';
  g.fillRect(0, 0, w, h);
  
  data = new O.ImageData(g);
  
  render();
};

const render = () => {
  const col1 = new Uint8ClampedArray([255, 0, 255]);
  const col2 = new Uint8ClampedArray([0, 255, 255]);
  
  const r = 30;
  
  const f = (x, y) => {
    const dist = O.hypot(x, y);
    const angle = -atan2(y, x);
    const distNew = dist + (angle / pi2 + 1) % 1 * r * 2;
    
    return distNew / r & 1;
  };
  
  data.iter((x, y) => {
    return f(x - wh, y - hh) ? col1 : col2;
  });
  
  data.put();
};

main();