'use strict';

const assert = require('assert');

const {min, max} = Math;
const {pi2} = O;

const radius = 30;
const smoothness = 2;
const distsNum = 5;
const rand = 1;
const initCircs = 1;
const initFill = 0;

const diam = radius * 2;

const mkCanvas = () => {
  const {g} = O.ceCanvas();
  const {canvas} = g;
  
  canvas.style.position = 'absolute';
  canvas.style.top = '0px';
  canvas.style.left = '0px';
  
  return [g, canvas];
};

const [gBg, canvasBg] = mkCanvas();
const [g, canvas] = mkCanvas();

const mkDists = () => {
  if(!rand){
    const dists = [
      [.25, .25],
      [.75, .25],
      [.25, .75],
      [.75, .75],
      [.5, .5],
    ];
    
    assert(dists.length === distsNum);
    return dists;
  }
  
  return O.ca(distsNum, () => O.ca(2, () => {
    return O.randf(-.5, .5);
  }));
};

const dists = mkDists();

let iw, ih, iwh, ihh;
let cx, cy;

let clicked = 0;

const main = () => {
  aels();
  onResize();
};

const aels = () => {
  O.ael('resize', onResize);
  O.ael('mousedown', onMouseDown);
  O.ael('mouseup', onMouseUp);
  O.ael('mousemove', onMouseMove);
};

const onResize = evt => {
  clicked = 0;
  
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;

  canvasBg.width = canvas.width = iw;
  canvasBg.height = canvas.height = ih;
  
  gBg.fillStyle = 'white';
  gBg.fillRect(0, 0, iw, ih);
  
  const imgd = new O.ImageData(gBg);
  const col = new Uint8ClampedArray(3);
  const iwhhMin = min(iwh, ihh);
  
  const distsLoc = dists.map(([x, y]) => {
    return [
      iwh + iwhhMin * x,
      ihh + iwhhMin * y,
    ];
  });
  
  imgd.iter((x, y) => {
    let facs;
    
    if(smoothness !== 0){
      const ds = distsLoc.map(([x1, y1]) => {
        return 1 / (1 + O.dists(x, y, x1, y1) ** smoothness);
      });
      
      const distsTotal = ds.reduce((a, b) => a + b, 0);
      facs = ds.map(a => a / distsTotal);
    }else{
      let dist, index;
      
      distsLoc.forEach(([x1, y1], i) => {
        const d = O.dist(x, y, x1, y1);
        
        if(i === 0 || d < dist){
          dist = d;
          index = i;
        }
      });
      
      facs = distsLoc.map((a, i) => {
        if(i === index) return 1;
        return 0;
      });
    }
    
    col[0] = (facs[0] + facs[4]) * 255;
    col[1] = (facs[1] + facs[2] + facs[4]) * 255;
    col[2] = (facs[2] + facs[3]) * 255;
    
    return col;
  });
  
  // g.globalCompositeOperation = 'lighten';
  // g.lineWidth = 5;
  
  imgd.put();
  
  g.lineWidth = diam;
  g.strokeStyle = 'black';
  g.lineCap = 'round';
  g.lineJoin = 'round';
  
  g.globalCompositeOperation = 'source-over';
  
  if(!initFill){
    g.fillStyle = 'white';
    g.fillRect(0, 0, iw, ih);
  }
  
  g.globalCompositeOperation = 'destination-out';
  g.fillStyle = 'black';
  
  if(initCircs){
    for(const [x, y] of distsLoc){
      g.beginPath();
      g.arc(x, y, radius, 0, pi2);
      g.fill();
    }
  }
};

const onMouseDown = evt => {
  updateCur(evt);
  clicked = 1;
  
  g.beginPath();
  g.arc(cx, cy, radius, 0, pi2);
  g.fill();
};

const onMouseUp = evt => {
  clicked = 0;
};

const onMouseMove = evt => {
  if(!clicked) return;
  
  g.beginPath();
  g.moveTo(cx, cy);
  updateCur(evt);
  g.lineTo(cx, cy);
  g.stroke();
};

const updateCur = evt => {
  cx = evt.clientX;
  cy = evt.clientY;
};

main();